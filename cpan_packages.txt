################################################################################
# From top cpan packages: https://metacpan.org/favorite/leaderboard
################################################################################
# ack # not found
AnyEvent
App::cpanminus
App::cpm
App::perlbrew
autodie
Capture::Tiny
Carp
Carton
Catalyst::Runtime
CHI
# Coro # error compiling
Dancer
Dancer2
Data::Dumper
Data::Printer
DateTime
# DBD::mysql # missing mysql_config
# DBD::Pg # missing PostGreSql
DBD::SQLite
DBI
DBIx::Class
Devel::Cover
Devel::NYTProf
Devel::REPL
# Dist::Zilla # needs libssl
Encode
Excel::Writer::XLSX
ExtUtils::MakeMaker
# FFI::Platypus # perl error
File::Slurp
File::Temp
Function::Parameters
Future
Getopt::Long
HTTP::Message
HTTP::Tiny
Imager
IO
IO::All
IO::Async
JSON
JSON::XS
# libwww::perl # not found
List::MoreUtils
local::lib
Log::Any
Log::Log4perl
MCE
Minilla
Minion
Module::CPANfile
Mojolicious
# Mojo::Pg # missing PostGreSql
# MongoDB # missing module BSON
Moo
# Moops # module Parse::Keyword
Moose
Mouse
Parallel::ForkManager
Path::Class
Path::Tiny
# PathTools # not found
# Paws # missing libssl
# perl # not found
Perl::Critic
Perl::Tidy
# Pinto # dep on Module Starman
Plack
POE
PPI
PSGI
Regexp::Common
Regexp::Debugger
Reply
# Rex # depends on XML::Simple
Role::Tiny
# Scalar::List::Utils # not found
# Sereal # compilation error
SQL::Abstract
# Starman # error in HTTP::Parser::XS
# Task::Kensho # various failures, Task::Kensho::XML
Template::Toolkit
Term::ANSIColor
Test::Harness
Test::Simple
Text::CSV
Text::CSV_XS
# Text::Xslate # fatal error C1189: #error:  msgpack-c supports only big endian and little endian
Time::HiRes
Time::Moment
Time::Piece
Try::Tiny
Type::Tiny
Ubic
URI
WWW::Mechanize
# XML::LibXML # missing libxml
# XML::Twig # module XML::Parser
YAML
################################################################################
# From strawberry perl: https://github.com/StrawberryPerl/Perl-Dist-Strawberry/blob/master/devel.utils/relnotes_test.pl
################################################################################
Algorithm::Diff
# Alien::Tidyp # build failed
App::Cpan
App::local::lib::Win32Helper
App::module::version
Archive::Extract
Archive::Tar
Archive::Zip
autodie
# BerkeleyDB # missing db.h
Capture::Tiny
Class::ErrorHandler
Class::Inspector
Class::Loader
common::sense
Compress::Bzip2
Compress::Raw::Bzip2
# Compress::Raw::Lzma # missing lzma.h
Compress::Raw::Zlib
Compress::unLZMA
Convert::ASCII::Armour
Convert::ASN1
Convert::PEM
CPAN::Checksums
CPAN::DistnameInfo
CPAN::Inject
CPAN::Meta
CPAN::Meta::Requirements
CPAN::Meta::YAML
CPAN::Mini
CPANPLUS
CPAN::SQLite
Crypt::Blowfish
Crypt::CAST5_PP
# Crypt::CBC # build failure, can't detect endianness
Crypt::DES
Crypt::DES_EDE3
# Crypt::DH # error dirent.h in package Math::Pari
Crypt::DSA
Crypt::IDEA
# Crypt::OpenPGP # Error on crypt twofish, missing header tables.h
# Crypt::Primes # error Math::Pari
# Crypt::Random # error Math::Pari
Crypt::Rijndael
Crypt::RIPEMD160
# Crypt::RSA # Math::Pari
# Crypt::SSLeay # missing libssl
# Crypt::Twofish # missing header tables.h
Data::Buffer
Data::Compare
Data::Random
DBD::ADO
# DBD::mysql # need mysql_config
DBD::ODBC
# DBD::Pg # missing PostGreSql
DBD::SQLite
# DB_File # missing db.h
DBI
DBIx::Simple
DBM::Deep
Digest::BubbleBabble
Digest::HMAC
Digest::MD2
Digest::SHA1
Encode::Locale
FCGI
File::chmod
File::Copy::Recursive
File::Fetch
File::Find::Rule
File::HomeDir
File::Listing
File::pushd
File::Remove
File::ShareDir
File::Slurp
File::Which
# Filter # can't be found?
# GD # missing gdlib config
HTML::Parser
HTML::Tagset
HTTP::Cookies
HTTP::Daemon
HTTP::Date
HTTP::Message
HTTP::Negotiate
HTTP::Tiny
Imager
# IO::Compress # can't be found
# IO::Compress::Lzma # missing lzma.h
IO::Interactive
# IO::Socket::SSL # missing libssl
IO::String
IO::Stringy
IPC::Cmd
IPC::Run
IPC::Run3
IPC::System::Simple
JSON
JSON::XS
# libwww::perl # not found
local::lib
LWP::MediaTypes
LWP::Online
# LWP::Protocol::https # missing ssl
# Math::BigInt::GMP # missing libgmp
# Math::GMP # strange error
# Math::MPC # missing libmpc
# Math::MPFR # missing libmpfr
# Math::Pari # missing dirent.h
Memoize
Module::Build
Module::Load::Conditional
Module::Signature
Mozilla::CA
Net::HTTP
# Net::SMTP::TLS # missing ssl
# Net::SSH2 # missing libssh
# Net::SSLeay # fix ssl
Number::Compare
Object::Accessor
# PAR # perl error?
Params::Check
Params::Util
PAR::Dist
# PAR::Dist::FromPPD # dep on XML::Parser
# PAR::Dist::InstallPPD # dep on XML::Parser
# PAR::Repository::Client # error compiling PAR
PAR::Repository::Query
Parse::Binary
Parse::CPAN::Meta
PerlIO::via::QuotedPrint
pip
pler
Portable
# PPM # dep on libssl + xmlparser
Probe::Perl
Safe
# Scalar::List::Utils # not found
Search::Dict
# SOAP::Lite # dep XML::Parser
 Socket
Sort::Versions
String::CRC32
Sub::Uplevel
Task::Weaken
Term::ANSIColor
# TermReadKey # not found
Term::ReadLine::Perl
Test::Deep
Test::Exception
Test::Manifest
Test::NoWarnings
Test::Script
Test::Tester
Test::Warn
Text::Diff
Text::Glob
# Tie::EncryptedHash # build failed cannot detect endianness
Tree::DAG_Node
URI
# Win32::API # error when compiling
Win32API::Registry
Win32::EventLog
# Win32::Exe # XML::Parser
Win32::File
Win32::File::Object
Win32::OLE
Win32::Process
Win32::TieRegistry
Win32::UTCFileTime
Win32::WinError
WWW::RobotRules
# XML::LibXML # need libxml
# XML::LibXSLT # need libxml
XML::NamespaceSupport
# XML::Parser # missing libexpat
XML::SAX
XML::SAX::Base
# XML::Simple # XML::Parser
YAML
YAML::Tiny
################################################################################
# Requested by some users
################################################################################
Getopt::Long
File::Copy
File::Path
Win32::Registry
Win32::IPC
Win32::ChangeNotify
